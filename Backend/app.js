/**
 * Created by krazibit on 9/16/2016.
 */
var config = require('./config.json');
var express = require('express');
var parser = require('body-parser');
var twilioClient = require('twilio')(config.twilioSID,config.twilioAuthToken);
var app = express();
//configure express to use body parse
app.use(parser.urlencoded({extended:true}));
app.use(parser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//configure routes


app.post('/api/sms-promotion', function(req,res){

    try {
        var phone = req.body.phone;
        console.log("got phone =>", phone);
        var smsBody = "";
        var now = new Date();
        if(now.getHours() > 11)
            smsBody = "Hello! Your promocode is PM456";
        else
            smsBody = "Good morning! Your promocode is AM123";

        var msg = generateMessage(smsBody,phone,config.fromNumber);
        console.log(msg);
        var responseMsg = {}
        twilioClient.sms.messages.create(msg,function(err,sms){

            if(err)
            {
                responseMsg.err = true;
                responseMsg.errMsg = handleError(err);
                responseMsg.success = false;
            }
            else
            {
                responseMsg.success = true;
            }

            res.jsonp(responseMsg);
        });


    }
    catch (e)
    {
        console.log(e.message);
        res.status(500)
            .jsonp("{'error':true, 'message': 'server error'}");
        return;
    }

});

app.listen(1636,function(){
    console.log('Server up!, running on port 1636');
});


function handleError(err)
{
    //Minimal error handling, for real app get twilio errorcodes/msg and process accordingly
    console.log(JSON.stringify(err))
    var code = err.code;
    switch (code)
    {
        case 21421 :
            return "Invalid Phone Number ";
        //server side errors to be handled
        case 20003 :
        default:
            return "Unable to process request now, please try later";

    }
}

function generateMessage(body,to,from)
{
    var message = {};
    message.body = body;
    message.to = to;
    message.from = from;
    return JSON.stringify(message);
}


